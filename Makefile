DOC_FILES=$(wildcard rfcs/*.rst) $(wildcard *.rst)

all:

check: lint

lint:
	rstcheck --report warning $(DOC_FILES)
